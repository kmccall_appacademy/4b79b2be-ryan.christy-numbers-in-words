class Fixnum

  HSH = {
   "0" => "zero",
   "1" => "one",
   "2" => "two",
   "3" => "three",
   "4" => "four",
   "5" => "five",
   "6" => "six",
   "7" => "seven",
   "8" => "eight",
   "9" => "nine",
   "10" => "ten",
   "11" => "eleven",
   "12" => "twelve",
   "13" => "thirteen",
   "14" => "fourteen",
   "15" => "fifteen",
   "16" => "sixteen",
   "17" => "seventeen",
   "18" => "eighteen",
   "19" => "nineteen",
   "20" => "twenty",
   "30" => "thirty",
   "40" => "forty",
   "50" => "fifty",
   "60" => "sixty",
   "70" => "seventy",
   "80" => "eighty",
   "90" => "ninety"
   }




def in_words
  new_str = []
  str = self.to_s
  return tnol(str) if self < 1000
  characs = self.to_s.delete('_').chars.reverse.each_slice(3).to_a.reverse.map(&:reverse)

  characs.each.with_index do |arr,idx|
    ridx = (characs.length - 1 - idx)
    new_str << tnol(arr.join(''))
    case ridx
      when 4
      new_str << "trillion"
      when 3
      new_str << "billion" if arr.join('') != "000"
      when 2
      new_str << "million" if arr.join('') != "000"
      when 1
      new_str << "thousand" if arr.join('') != "000"
    end
  end

  new_str.reject(&:empty?).join(' ').rstrip
end


def tnol(str)
  res_str = []

  case str.length
  when 1
    res_str << HSH[str]
  when 2
    res_str << tniw(str)
  when 3
    return '' if str == "000"
    res_str << HSH[str[0]] << "hundred" if str[0] != "0"
    res_str << tniw(str[1..2]) if str[1..2] != "00"
  end

  res_str.join(' ').rstrip
end

 #two numbers in words
 def tniw(str)
   res_str = []

   str.chars.each.with_index do |char,idx|

     case idx
       when 1
       res_str << HSH[char]
       break
       when 0
       next if char == "0"
       return HSH[str] if str.to_i <= 20
       res_str << HSH[(char.to_i * 10).to_s]
       break if str[idx+1] == "0"
     end

   end

   res_str.join(' ')
 end


end
